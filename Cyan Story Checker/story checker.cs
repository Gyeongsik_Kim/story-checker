﻿using System;
using System.Net;

/**
 * Author : GyungDal
 * Date : 2017-02-08
 **/

namespace Cyan_Story_Checker{
    class story_checker {
        public event EventHandler updateStoryHandler;
        public struct StoryData {
            public TimeSpan diff;
            public DateTime beforeStoryTime, nowStoryTime;
            public UInt32 nowStorysNumber;
        }
        private DateTime lastSyncStorysTime;
        private string uid;
        private UInt32 refreshTime, lastSyncStorysNumber;
        private System.Timers.Timer timer;
        /**
        * uid : User ID
        * time : refresh time (sec)
        **/
        public story_checker(string uid, UInt32 refreshTime) {
            this.uid = uid;
            this.refreshTime = refreshTime;
            lastSyncStorysNumber = 0;
        }

        //Notice : please use on thread
        public void start(){
            if(timer != null){
                Console.WriteLine("You alreay start");
                return;
            }
            timer = new System.Timers.Timer();
            timer.Interval = 1000 * refreshTime;
            timer.Elapsed += new System.Timers.ElapsedEventHandler(parsingData);
            timer.Start();
        }

        public void stop()
        {
            if(timer != null){
                timer.Stop();
                timer = null;
            }
        }

        private void parsingData(object sender, System.Timers.ElapsedEventArgs e){
            WebClient client = new WebClient();
            client.Headers.Add("x-kakao-apilevel:34");
            client.Headers.Add("x-kakao-deviceinfo:web:d;-;-");
            client.Headers.Add("x-kakao-vc: ");
            client.Headers.Add("x-requested-with:XMLHttpRequest");
            string ds = client.DownloadString("https://story.kakao.com/a/profiles/" + uid);
            ds = ds.Substring(ds.IndexOf("\"activity_count\":") + "\"activity_count\":".Length);
            ds = ds.Substring(0, ds.IndexOf(","));
            UInt32 storys = UInt32.Parse(ds);
            if (lastSyncStorysNumber == 0){
                lastSyncStorysTime = DateTime.Now;
                lastSyncStorysNumber = storys;
            }
            else if(lastSyncStorysNumber != storys){
                StoryData data = new StoryData();
                data.nowStoryTime = DateTime.Now;
                data.nowStorysNumber = storys;
                data.beforeStoryTime = lastSyncStorysTime;
                data.diff = data.nowStoryTime.Subtract(lastSyncStorysTime);
                lastSyncStorysTime = data.beforeStoryTime;
                lastSyncStorysNumber = storys;
                updateStoryHandler(data, null);
            }
        }
    }
}
