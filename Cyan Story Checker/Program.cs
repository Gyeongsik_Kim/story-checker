﻿using System;
using System.IO;
using System.Net;
using System.Threading;
namespace Cyan_Story_Checker
{
    class Program
    {
        static void Main(string[] args)
        {
            story_checker story = new story_checker("aa9274", 1);
            story.updateStoryHandler += test;
            new Thread(story.start).Start();
            while (true) ;
        }

        private static void test(object s, EventArgs e)
        {
            story_checker.StoryData data = (story_checker.StoryData)s;
            Console.WriteLine("-----------------------------------------------");
            Console.WriteLine("이전 글 업로드 시간(또는 프로그램 시작 시간) : " + data.beforeStoryTime.ToLongTimeString());
            Console.WriteLine("현재 글 업로드 시간 : " + data.nowStoryTime.ToLongTimeString());
            Console.WriteLine("현재 글 개수 : " + data.nowStorysNumber);
            Console.WriteLine("이전 글과의 시간 차이 : " + data.diff.ToString());
            Console.WriteLine("------------------------------------------   -----");
            Console.WriteLine();
        }

        private static void test2()
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(new Uri("https://accounts.kakao.com/weblogin/authenticate"));
            request.Method = "POST";

            // init your request...then:
            request.Timeout = 3000;
            DoWithResponse(request, (response) => {
                var body = new StreamReader(response.GetResponseStream()).ReadToEnd();
                Console.Write(body);
            });
        }

        static void DoWithResponse(HttpWebRequest request, Action<HttpWebResponse> responseAction)
        {
            Action wrapperAction = () =>
            {
                request.BeginGetResponse(new AsyncCallback((iar) =>
                {
                    var response = (HttpWebResponse)((HttpWebRequest)iar.AsyncState).EndGetResponse(iar);
                    responseAction(response);
                }), request);
            };
            wrapperAction.BeginInvoke(new AsyncCallback((iar) =>
            {
                var action = (Action)iar.AsyncState;
                action.EndInvoke(iar);
            }), wrapperAction);
        }
    }
}
